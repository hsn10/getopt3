/*
 * Copyright (c) Radim Kolar 2013, 2018, 2023.
 * SPDX-License-Identifier: MIT
 *
 * getopt3 library is licensed under MIT license:
 *   https://spdx.org/licenses/MIT.html
*/

use super::*;

/** validate if all optstring characters are valid and string is not empty */
fn qc_valid_optstring(s: &str) -> bool {
   // string can't be empty
   if (s.len() == 0) { return false; }
   // string can't start with ':'
   if let Some(':') = s.chars().next() { return false; }
   // string can't contain double '::'
   if (s.contains("::")) { return false; }

   for c in s.chars() {
      match c {
         '0'..='9'|'a'..='z'|'A'..='Z' => (),
         ':' => (),
          _ => return false
      }
   };
   true
}

#[test]
fn quick_validator_fails_on_empty_input() {
   assert!( !qc_valid_optstring("") );
}

#[test]
fn quick_validator_fails_on_semicolon_only() {
   assert!( !qc_valid_optstring(":") );
}

#[test]
fn quick_validator_fails_on_double_semicolon() {
   assert!( !qc_valid_optstring("a::") );
   assert!( !qc_valid_optstring("::") );
   assert!( !qc_valid_optstring("a::b") );
}

#[test]
fn quick_validator_passes_on_simple_semicolon() {
   assert!( qc_valid_optstring("ab:c") );
}

quickcheck! {
   fn quick_validator_and_validate_optstring_matches(xs: String) -> bool {
      qc_valid_optstring(&xs) == validate_optstring(&xs).is_ok()
   }
}

#[allow(unused_macros)]

macro_rules! quickcheck_filter { 
   () => ()

}