/*
 * Copyright (c) Radim Kolar 2013, 2018, 2023.
 * SPDX-License-Identifier: MIT
 *
 * getopt3 library is licensed under MIT license:
 *   https://spdx.org/licenses/MIT.html
*/

/* testsuite for validate() API function */

use crate::{new, validate};

/** converts iterable &str into String Vec */
fn toStringVec<'a>(iter: impl IntoIterator<Item = &'a str>) -> Vec<String> {
    iter.into_iter().map(|s| s.to_string()).collect()
}


#[test]
fn validate_unknown_option_without_argument() {
   let g = new(toStringVec( ["one","two", "-c", "-a"]), "a");
   assert! ( g.is_ok() );
   assert! ( validate(g.unwrap()).is_err());
}

#[test]
fn validate_unknown_option_with_argument() {
   let g = new(toStringVec( ["one","two", "-c", "kalo"]), "a");
   assert! ( g.is_ok() );
   assert! ( validate(g.unwrap()).is_err());
}

#[test]
fn validate_required_argument_missing() {
   let g = new(toStringVec( ["one","two", "-c"]), "c:");
   assert! ( g.is_ok() );
   assert! ( validate(g.unwrap()).is_err());
}

#[test]
fn validate_no_options_on_cmdline_but_options_defined() {
   let g = new(toStringVec( ["one","two", "three"]), "abc:");
   assert! ( g.is_ok() );
   let g = validate(g.unwrap());
   assert! ( g.is_ok());
   assert_eq! ( g.unwrap().arguments.len(), 3);
}
