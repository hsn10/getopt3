/*
 * Copyright (c) Radim Kolar 2013, 2018, 2023.
 * SPDX-License-Identifier: MIT
 *
 * getopt3 library is licensed under MIT license:
 *   https://spdx.org/licenses/MIT.html
*/

/*  Tests for double dash -- POSIX standard */

use crate::new;

/** converts iterable &str into String Vec */
fn toStringVec<'a>(iter: impl IntoIterator<Item = &'a str>) -> Vec<String> {
    iter.into_iter().map(|s| s.to_string()).collect()
}


#[test]
fn gnu_no_options_dash_in_middle() {
   let g = new( [ "one","--","two" ], "a");
   assert! ( g.is_ok() );
   let g = g.unwrap();
   assert_eq! ( g.arguments, toStringVec(["one","two"]) );
}

#[test]
fn gnu_no_options_dash_at_end() {
   let g = new( ["one","two","--"], "a");
   assert! ( g.is_ok() );
   let g = g.unwrap();
   assert_eq! ( g.arguments, toStringVec(["one","two"]) );
}

#[test]
fn gnu_options_with_dash_in_middle() {
   let g = new( ["one","two","--","-a"], "a");
   assert! ( g.is_ok() );
   let g = g.unwrap();
   assert_eq! ( g.arguments, toStringVec(["one","two","-a"]) );
}

#[test]
fn gnu_options_after_dash() {
   let g = new( ["--","-a2"], "a:");
   assert! ( g.is_ok() );
   let g = g.unwrap();
   assert_eq! ( g.arguments, toStringVec(["-a2"]) );
}

#[test]
fn gnu_dash_following_option_with_argument() {
   let g = new( ["one","two","-c","--","end"], "c:");
   assert! ( g.is_ok() );
   let g = g.unwrap();
   assert_eq! ( g.arguments, toStringVec(["one","two","end"]) );
   assert_eq! ( g.options.get(&'c'), Some(&"--".to_string()) );
}

#[test]
fn gnu_dash_following_option_without_argument() {
   let g = new( ["one","two","-c","--","end"], "c");
   assert! ( g.is_ok() );
   let g = g.unwrap();
   assert_eq! ( g.arguments, toStringVec(["one","two","end"]) );
   assert_eq! ( g.options.get(&'c'), Some(&"".to_owned()) );
}
