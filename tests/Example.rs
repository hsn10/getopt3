/*
 * Copyright (c) Radim Kolar 2013, 2018, 2023.
 * SPDX-License-Identifier: MIT
 *
 * getopt3 library is licensed under MIT license:
 *   https://spdx.org/licenses/MIT.html
*/

#![allow(non_snake_case)]

use getopt3::{getopt, new};

/**
Example how to use getopt3 crate in your program.

Function parses command line arguments using hardcoded option string
and returns *getopt* struct. Panics on parse error which should not
happen because our option string is valid.

### Arguments
   * args Command line options to parse
*/
pub fn Example(args: Vec<String>) -> getopt {
   let g = new(args, "ab:c?");
   g.unwrap()
}
