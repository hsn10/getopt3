/*
 * Copyright (c) Radim Kolar 2013, 2018, 2023.
 * SPDX-License-Identifier: MIT
 *
 * getopt3 library is licensed under MIT license:
 *   https://spdx.org/licenses/MIT.html
*/

#![allow(non_snake_case)]

/** check parsing using real world environment */
#[test]
pub fn main() {
   let rc = getopt3::new(std::env::args(), "ab:c");
   assert! (rc.is_ok());
   let g = rc.unwrap();
   assert! (getopt3::validate(g).is_ok());
}
