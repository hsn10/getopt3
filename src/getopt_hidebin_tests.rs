/*
 * Copyright (c) Radim Kolar 2024.
 * SPDX-License-Identifier: MIT
 *
 * getopt3 library is licensed under MIT license:
 *   https://spdx.org/licenses/MIT.html
*/

use super::hideBin;

/** converts iterable &str into String Vec */
fn toStringVec<'a>(iter: impl IntoIterator<Item = &'a str>) -> Vec<String> {
   iter.into_iter().map(|s| s.to_string()).collect()
}

#[test]
fn comparing_iterators_works() {
   let i1 = toStringVec(["one", "two", "three"]).into_iter();
   let i2 = toStringVec(["one", "two", "three"]).into_iter();
   assert! ( i1.eq(i2) );

   let i5 = toStringVec(["one", "two", "five"]).into_iter();
   let i3 = toStringVec(["three", "three", "two"]).into_iter();
   assert! ( ! i3.eq(i5) );
}

#[test]
fn zero_size() {
   let v1 = toStringVec([]);
   assert_eq! ( v1.len(), 0);
   let v2 = hideBin(v1);
   assert_eq! ( v2.into_iter().count(), 0);

   let v1again = toStringVec([]);
   let v2again = hideBin(v1again);
   assert_eq! ( v2again.into_iter().next(), None);
}

#[test]
fn one_size() {
   let v1 = toStringVec(["binary"]);
   assert_eq! ( v1.len(), 1);
   let v2 = hideBin(v1);
   assert_eq! ( v2.into_iter().count(), 0);

   let v1again = toStringVec(["binary"]);
   let v2again = hideBin(v1again);
   assert_eq! ( v2again.into_iter().next(), None);
}

#[test]
fn first_consumed() {
   let v1 = toStringVec(["binary","one","two"]);
   assert_eq! ( v1.len(), 3);
   let v2 = hideBin(v1);
   assert_eq! ( v2.into_iter().count(), 2);

   let v1again = toStringVec(["binary","one","two"]);
   let v2again = hideBin(v1again);
   let mut v2i = v2again.into_iter();
   assert_eq! ( v2i.next(), Some("one".to_string()));
   assert_eq! ( v2i.next(), Some("two".to_string()));
   assert_eq! ( v2i.next(), None);
}
