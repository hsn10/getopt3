/*
 * Copyright (c) Radim Kolar 2013, 2018, 2023.
 * SPDX-License-Identifier: MIT
 *
 * getopt3 library is licensed under MIT license:
 *   https://spdx.org/licenses/MIT.html
*/

#![allow(non_snake_case)]

mod Example;
use getopt3::getopt;

macro_rules! to_vec {
   ($arr:expr) => {{
      let vec: Vec<String> = $arr.iter().map(|&s| s.to_string()).collect();
      vec
   }};
}

// Example spec

use Example::Example;

/** getopt instance for tests */
fn main() -> getopt {
   Example(to_vec!(["one", "-a", "two", "-b", "arg", "-d"]))
}

#[test]
fn real_invoke_main() {
   Example(to_vec!(["one", "-a", "two", "-b", "arg", "-d"]));
}

#[test]
fn real_two_arguments() {
   let main = main();
   assert_eq!(main.arguments.len(), 2);
   assert_eq!(main.arguments, ["one", "two"]);
}

#[test]
fn real_a_option_without_arg() {
   let main = main();
   assert_eq!(main.options.get(&'a'), Some(&"".to_string()));
}

#[test]
fn real_should_have_b_option_with_argument() {
   let main = main();
   assert_eq!(main.options.get(&'b'), Some(&"arg".to_string()));
}

#[test]
fn real_should_not_have_c_option() {
   let main = main();
   assert_eq!(main.options.get(&'c'), None);
}

#[test]
fn real_should_have_unknown_d_option() {
   let main = main();
   assert_eq!(main.options.get(&'d'), Some(&"".to_string()));
}

#[test]
fn real_invoked_help() {
   let main = Example(to_vec!(["-?"]));
   assert_eq!(main.options.get(&'?'), Some(&"".to_string()));
}
