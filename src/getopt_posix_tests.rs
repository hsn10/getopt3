/*
 * Copyright (c) Radim Kolar 2013, 2018, 2023.
 * SPDX-License-Identifier: MIT
 *
 * getopt3 library is licensed under MIT license:
 *   https://spdx.org/licenses/MIT.html
*/

use super::posix;


//  basic getopt parsing

/** converts iterable &str into String Vec */
fn toStringVec<'a>(iter: impl IntoIterator<Item = &'a str>) -> Vec<String> {
    iter.into_iter().map(|s| s.to_string()).collect()
}

#[test]
fn basic_no_options() {
   let g = posix( ["one","two"], "a");
   assert! ( g.is_ok() );
   let g = g.unwrap();
   assert_eq! ( g.arguments.len(), 2 );
   assert_eq! ( g.options.len(), 0 );
}

#[test]
fn basic_one_option_without_arg() {
   let g = posix( ["-a","one","two"], "a");
   assert! ( g.is_ok() );
   let g = g.unwrap();
   assert_eq! ( g.arguments.len(), 2 );
   assert_eq! ( g.options.len(), 1 );
   assert_eq! ( g.options.get(&'a'), Some(&"".to_string()) );
}

#[test]
fn basic_one_option_in_middle_without_arg() {
   let g = posix( ["one","-a","two"], "a");
   assert! ( g.is_ok() );
   let g = g.unwrap();
   assert_eq! ( g.arguments.len(), 3 );
   assert_eq! ( g.options.len(), 0 );
}

#[test]
fn basic_one_option_with_arg() {
   let g = posix( ["-a","two","one"], "a:");
   assert! ( g.is_ok() );
   let g = g.unwrap();
   assert_eq! ( g.arguments.len(), 1 );
   assert_eq! ( g.options.len(), 1 );
   assert_eq! ( g.options.get(&'a'), Some(&"two".to_string()) );
   assert_eq! ( g.arguments, toStringVec(["one"]) );
}

#[test]
fn basic_one_option_in_middle_with_arg() {
   let g = posix(["one","-a","two"], "a:");
   assert! ( g.is_ok() );
   let g = g.unwrap();
   assert_eq! ( g.arguments.len(), 3 );
   assert_eq! ( g.options.len(), 0 );
}


#[test]
fn basic_two_options_without_arg() {
   let g = posix( ["-a","-b","one","two"], "ab");
   assert! ( g.is_ok() );
   let g = g.unwrap();
   assert_eq! ( g.options.get(&'a'), Some(&"".to_string()) );
   assert_eq! ( g.options.get(&'b'), Some(&"".to_string()) );
   assert_eq! ( g.arguments, toStringVec(["one","two"]) );
}

#[test]
fn basic_two_options_without_arg_in_middle() {
   let g = posix( ["one","-a","-b","two"], "ab");
   assert! ( g.is_ok() );
   let g = g.unwrap();
   assert_eq! ( g.arguments.len(), 4 );
   assert_eq! ( g.options.len(), 0 );
}


#[test]
fn basic_option_with_missing_arg() {
   let g = posix( ["-c"], "c:");
   assert! ( g.is_ok() );
   let g = g.unwrap();
   assert_eq! ( g.arguments.len(), 0 );
   assert_eq! ( g.options.get(&'c'), Some(&"".to_string()) );
}


//  advanced getopt parsing

#[ignore]
fn advanced_one_with_arg_in_same_element() {
   let g = posix(toStringVec( ["one","-atwo","three"]), "a:");
   assert! ( g.is_ok() );
   let g = g.unwrap();
   assert_eq! ( g.arguments, toStringVec(["one","three"]) );
   assert_eq! ( g.options.get(&'a'), Some(&"two".to_string()) );
}

#[ignore]
fn advanced_two_options_together_without_arg() {
   let g = posix(toStringVec( ["one","-ab","two"]), "ab");
   assert! ( g.is_ok() );
   let g = g.unwrap();
   assert_eq! ( g.arguments, toStringVec(["one","two"]) );
   assert_eq! ( g.options.get(&'a'), Some(&"".to_string()) );
   assert_eq! ( g.options.get(&'b'), Some(&"".to_string()) );
}

#[test]
fn advanced_dash_without_option() {
   let g = posix( ["one","-","two"], "ab");
   assert! ( g.is_ok() );
   let g = g.unwrap();
   assert_eq! ( g.arguments, toStringVec(["one","-","two"]) );
   assert_eq! ( g.options.len(), 0 );
}

#[ignore]
fn advanced_two_options_with_arg_in_same_element() {
   let g = posix( ["-abtwo","one","three"], "ab:");
   assert! ( g.is_ok() );
   let g = g.unwrap();
   assert_eq! ( g.arguments, toStringVec(["one","three"]) );
   assert_eq! ( g.options.get(&'a'), Some(&"".to_string()) );
   assert_eq! ( g.options.get(&'b'), Some(&"two".to_string()) );
}


// unknown options parsing

#[test]
fn unknown_option_without_arg() {
   let g = posix( ["-c","-a","one","two"], "a");
   assert! ( g.is_ok() );
   let g = g.unwrap();
   assert_eq! ( g.arguments, toStringVec(["one","two"]) );
   assert_eq! ( g.options.get(&'a'), Some(&"".to_string()) );
   assert_eq! ( g.options.get(&'c'), Some(&"".to_string()) );
}

#[test]
fn unknown_option_with_arg() {
   let g = posix( ["-c","one","two","kalo"], "a");
   assert! ( g.is_ok() );
   let g = g.unwrap();
   assert_eq! ( g.arguments, toStringVec(["one","two","kalo"]) );
   assert_eq! ( g.options.get(&'c'), Some(&"".to_string()) );
}

// misc tests

#[test]
fn bad_input_zero_sized_strings() {
   let g = posix( ["one","two","-c",""], "a");
   assert! ( g.is_ok() );
}